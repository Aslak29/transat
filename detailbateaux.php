<?php


$idSkipper = 0;
if (isset($_GET['idSkipper'])) {
    $idSkipper = intval(htmlspecialchars($_GET['idSkipper']));
}



require 'bdd/bddconfig.php';
try {
    $objBdd = new PDO("mysql:host=$bddserver; dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(
        PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION
    );

    $listeSkipper = $objBdd->prepare("SELECT nomSkipper, photo FROM skipperWHERE idBateau = 1");
    $listeSkipper->bindParam(':id', $idSkipper, PDO::PARAM_INT);
    $listeSkipper->execute();
} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}
?>

<?php $titre = "Acceuil"; ?>
<?php ob_start();
session_start();
?>

<ul>
    <?php foreach ($listeSkipper as $skipper) { ?>
        <li><span><?= $skipper['nomSkipper']; ?></span></a>
            <span><?= $skipper['photo']; ?></span></a>
        </li>
    <?php } ?>
</ul>

<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php'; ?>