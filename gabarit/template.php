<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Bateaux</title>
    <link rel="stylesheet" href="css/transat.css" />
</head>

<body>
    <div id="conteneur">
        <header>
            <div class="login"><?php
                                if (isset($_SESSION['logged_in']['login']) == TRUE) {
                                    //l'internaute est authentifié
                                    echo 'Salut à toi, ' . $_SESSION['logged_in']['pseudo'] . '';
                                    //affichage "se déconnecter"(logout.php), "prif", "paramètres", etc.
                                ?><a href="logout.php">Se déconnecter</a><?php
                                                                        } else {
                                                                            //Personne n'est authentifié
                                                                            //affichage d'un lien pour se connecter
                                                                            ?>
                    <a href="login.php">Connexion</a>
                <?php
                                                                        }

                ?>
            </div>

        </header>

        <nav>
            <ul>
                <li><a href="index.php">Accueil</a></li>
                <li><a href="classements.php">Classement</a></li>

                <?php if (isset($_SESSION['logged_in']['login']) == TRUE) { ?>
                    <li><a href="ajoutclasse.php">Ajouter Classe</a></li>
                <?php } ?>

            </ul>
        </nav>

        <section>
            <?php echo $contenu; ?>

        </section>

        <footer>
            <p>Copyright TruitesPHP - Tous droits réservés -
                <a href="#">Contact</a>
            </p>
        </footer>
    </div>
</body>

</html>