<?php


$idClasse = 0;
if (isset($_GET['idClasse'])) {
    $idClasse = intval(htmlspecialchars($_GET['idClasse']));
}



require 'bdd/bddconfig.php';
try {
    $objBdd = new PDO("mysql:host=$bddserver; dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(
        PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION
    );

    $listeBateaux = $objBdd->prepare("SELECT * FROM bateau WHERE idClasse=$idClasse");
    $listeBateaux->bindParam(':id', $idClasse, PDO::PARAM_INT);
    $listeBateaux->execute();
} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}
?>

<?php $titre = "Acceuil"; ?>
<?php ob_start();
session_start();
?>

<ul>
    <?php foreach ($listeBateaux as $bateau) { ?>
        <li><a href="detailbateaux.php?idSkipper=<?= $bateau['idClasse']; ?>">
                <span><?= $bateau['nomBateau']; ?></span></a>
        </li>
    <?php } ?>
</ul>

<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php'; ?>