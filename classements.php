<?php $titre = "Classe"; ?>
<?php session_start(); ?>
<?php
require "bdd/bddconfig.php";
try {
    $objBdd = new PDO(
        "mysql:host=$bddserver;
   dbname=$bddname;
   charset=utf8",
        $bddlogin,
        $bddpass
    );
    $objBdd->setAttribute(
        PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION
    );

    $classementBateau = $objBdd->query("SELECT * FROM classebateau");
} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}
?>
<?php ob_start(); ?>
<article>
    <h1>Classe</h1>
    <?php
    while ($classement = $classementBateau->fetch()) {
    ?>

        <h2><a href="listebateaux.php?idClasse=<?php echo $classement['idClasse']; ?>"><?php echo $classement['nomClasse']; ?></a></h2>

        <p><?= $classement['typeCoque']; ?></p>


    <?php
    } //fin du while
    $classementBateau->closeCursor(); //libère les ressources de la bdd
    ?>

</article>
<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php'; ?>